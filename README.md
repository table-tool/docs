# TableTool
### Сервис поиска предложений продажи настольных игр и игровых встреч "TableTool"

Данный проект выполняют студенты ФКН 3 курса группы 6.1. (Команда 1)

* Сметанин И. Ю.
* Соболев М. К.
* Борисенков В. Э

# Сервисы для работы
* [Trello](https://trello.com/b/m7dDawyv/%D0%BD%D0%B0%D1%81%D1%82%D0%BE%D0%BB%D0%BA%D0%B8)
* [Miro](https://miro.com/app/board/uXjVOHxvaJk=/)


# Android приложение

- [Ссылка на apk (gitlab)]
- [Ссылка на apk (gdrive)]

Данные для входа (почта и пароль):

- user@mail.ru
- useruser

# Документация
- [Swagger] - Ссылка ведёт на backend сервер, развёрнутый на heroku вместе со swagger документацией. Следует подождать в районе 40 секунд и перезагрузить страницу, если не произошло автоматической перезагрузки. Это происходит из-за того, что бесплатный воркер на heroku засыпает, если к нему не обращаться в течение 30 минут
- [SwaggerAuth] - По аналогии с верхней ссылкой. Swagger документация сервера с реализованной аутентификацией при помощи JWT токена. Токена с неистекающим сроком действия создать не удалось, поэтому для тестирования работы API можно пользоваться первой ссылкой

---

- [Техническое задание]
- [Курсовой проект]
- [Презентация]

---

- [Видео с презентацией]
- [Видео с технической реализацией]

# Репозитории с кодом
- [Frontend] 
- [Backend] 

[Swagger]: <https://table-tool.herokuapp.com/swagger-ui.html>
[SwaggerAuth]: <https://table-tool-dev.herokuapp.com/swagger-ui.html>
[Техническое задание]: <https://gitlab.com/table-tool/docs/-/blob/main/docs/%D0%A2%D0%B5%D1%85%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B5%20%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5.pdf>
[Курсовой проект]: <https://gitlab.com/table-tool/docs/-/blob/main/docs/%D0%9A%D1%83%D1%80%D1%81%D0%BE%D0%B2%D0%BE%D0%B9%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82.pdf>
[Презентация]: <https://gitlab.com/table-tool/docs/-/blob/main/docs/%D0%9F%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F.pdf>
[Frontend]: <https://gitlab.com/table-tool/frontend>
[Backend]: <https://gitlab.com/table-tool/backend>

[Видео с технической реализацией]: <https://www.youtube.com/watch?v=w-xb5tODGEg>
[Видео с презентацией]: <https://www.youtube.com/watch?v=EkZVFjvaFIU>


[Ссылка на apk (gitlab)]: <https://gitlab.com/table-tool/docs/-/blob/main/docs/TableTool.apk>

[Ссылка на apk (gdrive)]: <https://drive.google.com/file/d/1zBNG665kfer4Ap8lvcdkylFiML4LvTqB>